import { defineConfig } from 'vite'
import postcss from 'rollup-plugin-postcss';
import postcssLit from 'rollup-plugin-postcss-lit';
import autoprefixer from 'autoprefixer';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    postcss({
      inject: false,
      plugins: [autoprefixer]
    }),
    postcssLit({importPackage: 'lit'})
  ],
  build: {
    lib: {
      entry: 'src/',
      formats: ['es']
    },
    rollupOptions: {
      external: /^lit/,
    }
  }
})
