import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'

import './JButton'

export default {
  title: 'Button',
  render: (args) => html`<j-button>${args.label}</j-button>`,
} as Meta

export const ButtonStory: StoryObj = {
  name: 'j-button',
  args: {
    label: 'button',
  },
}