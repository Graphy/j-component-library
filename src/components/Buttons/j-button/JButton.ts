import { LitElement, html, unsafeCSS } from 'lit'
import { customElement, property } from 'lit/decorators.js'
import Style from "./JButton.scss?inline"

@customElement('j-button')
export class JButton extends LitElement {
  
  static styles = unsafeCSS(Style);

  /**
   * The text in the button.
   */
   @property()
   label = 'button'

  render() {
    return html`
        <button part="button">
          ${this.label}
        </button>
    `
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'j-button': JButton
  }
}
