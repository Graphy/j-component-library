import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'

import './JTextArea'

export default {
  title: 'TextArea',
  render: (args) => html`<j-textarea placeholder=${args.placeholder} value=${args.value}></j-textarea>`,
} as Meta

export const TextAreaStory: StoryObj = {
  name: 'j-textarea',
  args: {
    placeholder: 'textarea',
    value: ""
  },
}