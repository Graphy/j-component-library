import { LitElement, html, unsafeCSS } from 'lit'
import { customElement, property } from 'lit/decorators.js'
import Styles from "./JTextArea.scss?inline"
/**
 *
 * @csspart textarea - The textarea
 */
@customElement('j-textarea')
export class TextArea extends LitElement {
  
  static styles = unsafeCSS(Styles);
  
  /**
   * The placeholder before input is given.
   */
   @property()
   placeholder = 'text-area'
  
   /**
   * The start value.
   */
   @property()
   value = ''

  render() {
    return html`
      <textarea placeholder=${this.placeholder} part="textarea" .value=${this.value}></textarea>
    `
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'j-textarea': TextArea
  }
}
