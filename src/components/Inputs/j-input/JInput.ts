import { LitElement, html, unsafeCSS } from 'lit'
import { customElement, property } from 'lit/decorators.js'
import Styles from "./JInput.scss?inline"
/**
 *
 * @csspart input - The input
 */
@customElement('j-input')
export class Input extends LitElement {
  
  static styles = unsafeCSS(Styles);
  
  /**
   * The placeholder before input is given.
   */
   @property()
   placeholder = 'Input'

  /**
   * The start value.
   */
   @property()
   value = ''

  render() {
    return html`
      <input placeholder=${this.placeholder} part="input" .value=${this.value}></input>
    `
  }

}

declare global {
  interface HTMLElementTagNameMap {
    'j-input': Input
  }
}
