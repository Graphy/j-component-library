import { Meta, StoryObj } from '@storybook/web-components'
import { html } from 'lit'

import './JInput'

export default {
  title: 'Input',
  render: (args) => html`<j-input placeholder=${args.placeholder} value=${args.value}></j-input>`,
} as Meta

export const InputStory: StoryObj = {
  name: 'j-input',
  args: {
    placeholder: 'input',
    value: ""
  },
}